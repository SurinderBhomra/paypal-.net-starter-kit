﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PayPal;
using PayPal.Api;
using PayPal.Api.Payments;
using SB.Paypal.Enums;
using SB.Paypal.Models;
using SB.Paypal.Utilities;

namespace SB.Paypal
{
    public class CreditCardTransaction
    {
        private readonly string _invoiceId;
        private readonly PersonalDetails _personalDetails;
        private readonly CurrencyEnum _currency;
        private readonly CardInfo _paymentCard;
        private readonly List<Product> _products;
        private readonly decimal _tax;
        private readonly decimal _delivery;

        /// <summary>
        /// Creates a card payment where no delivery or tax is required.
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="personalDetails"></param>
        /// <param name="currency"></param>
        /// <param name="products"></param>
        /// <param name="creditCard"></param>
        public CreditCardTransaction(string invoiceId, 
                                    PersonalDetails personalDetails, 
                                    CurrencyEnum currency, 
                                    List<Product> products, 
                                    CardInfo creditCard)
        {
            _invoiceId = invoiceId;
            _personalDetails = personalDetails;
            _currency = currency;
            _tax = 0;
            _delivery = 0;
            _paymentCard = creditCard;
            _products = products;
        }

        /// <summary>
        /// Creates a card payment that allows all information on the order to be stated.
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <param name="personalDetails"></param>
        /// <param name="currency"></param>
        /// <param name="tax"></param>
        /// <param name="delivery"></param>
        /// <param name="products"></param>
        /// <param name="creditCard"></param>
        public CreditCardTransaction(string invoiceId, 
                                    PersonalDetails personalDetails, 
                                    CurrencyEnum currency,
                                    decimal tax,
                                    decimal delivery, 
                                    List<Product> products, 
                                    CardInfo creditCard)
        {
            _invoiceId = invoiceId;
            _personalDetails = personalDetails;
            _currency = currency;
            _tax = tax;
            _delivery = delivery;
            _paymentCard = creditCard;
            _products = products;
        }

        public PaypalPayment SendPayment()
        {
            #region Get products

            ItemList itemList = new ItemList();

            if (_products.Any())
            {
                List<Item> payPalItems = _products.Select(p => new Item
                {
                    name = p.Name, 
                    currency = _currency.ToString(), 
                    price = p.Price.ToString("N"), 
                    quantity = p.Quantity.ToString(), 
                    sku = p.Sku
                }).ToList();

                itemList.items = payPalItems;
            }

            #endregion

            #region Personal Details
            
            // Address for the payment
            Address billingAddress = new Address();

            if (_personalDetails != null)
            {
                billingAddress.line1 = _personalDetails.Address1;
                billingAddress.line2 = _personalDetails.Address2;
                billingAddress.state = _personalDetails.County;
                billingAddress.city = _personalDetails.City;
                billingAddress.postal_code = _personalDetails.PostCode;
                billingAddress.country_code = _personalDetails.CountryCode;
            }

            #endregion

            #region Credit Card

            CreditCard creditCard = new CreditCard();

            if (_paymentCard != null)
            {
                creditCard.billing_address = billingAddress;
                creditCard.cvv2 = _paymentCard.CVV2;
                creditCard.expire_month = _paymentCard.ExpiryMonth;
                creditCard.expire_year = _paymentCard.ExpireYear;
                creditCard.first_name = (_personalDetails != null) ? _personalDetails.FirstName : String.Empty;
                creditCard.last_name = (_personalDetails != null) ? _personalDetails.LastName : String.Empty;
                creditCard.number = _paymentCard.Number;
                creditCard.type = _paymentCard.CardType.ToString().ToLower();
            }

            #endregion

            #region Payment Amount Details

            Details paymentDetails = new Details();
            paymentDetails.shipping = _delivery.ToString();
            paymentDetails.subtotal = _products.Sum(p => p.Total).ToString("N");
            paymentDetails.tax = _tax.ToString();

            Amount paymentAmount = new Amount();
            paymentAmount.currency = _currency.ToString();

            // Total must be equal to sum of shipping, tax and subtotal.
            paymentAmount.total = ((decimal.Parse(paymentDetails.subtotal) * (100 + _tax) / 100) + _delivery).ToString("N");
            paymentAmount.details = paymentDetails;

            #endregion

            #region Transaction

            // A transaction defines the contract of a payment - what is the payment for and who is fulfilling it. 
            Transaction trans = new Transaction();
            trans.amount = paymentAmount;
            trans.description = "Creating a direct payment with card.";
            trans.item_list = itemList;
            trans.invoice_number = _invoiceId;

            // The Payment creation API requires a list of transactions.
            List<Transaction> transactions = new List<Transaction>
            {
                trans
            };

            #endregion

            #region Funding

            // A resource representing a Payer's funding instrument. For direct credit card payments, set the CreditCard field on this object.
            FundingInstrument fundInstrument = new FundingInstrument();
            fundInstrument.credit_card = creditCard;

            List<FundingInstrument> fundingInstrumentList = new List<FundingInstrument>();
            fundingInstrumentList.Add(fundInstrument);

            #endregion

            #region Payer

            // A resource representing a Payer that funds a payment. Use the List of 'FundingInstrument' and the Payment Method as 'credit_card'.
            Payer payer = new Payer
            {
                funding_instruments = fundingInstrumentList, 
                payment_method = "credit_card",
                payer_info = new PayerInfo()
                {
                    billing_address = billingAddress,
                    email = (_personalDetails != null) ? _personalDetails.EmailAddress : String.Empty
                }
            };

            // A Payment Resource; create one using the above types and intent as 'sale' or 'authorize'.
            Payment payment = new Payment
            {
                intent = "sale",
                payer = payer, 
                transactions = transactions
            };

            #endregion

            try
            {
                // Create a payment using a valid APIContext.
                Payment createdPayment = payment.Create(Configuration.GetAPIContext());

                return new PaypalPayment
                {
                    Id = createdPayment.id,
                    InvoiceId = createdPayment.transactions[0].invoice_number,
                    Message = createdPayment.intent,
                    CreatedTime = DateTime.Parse(createdPayment.create_time),
                    Status = "success",
                    UpdatedTime = DateTime.Parse(createdPayment.update_time)
                };
            }
            catch (PayPal.Exception.PayPalException ex)
            {
                RequestError error = RestResponse.Error(((PayPal.Exception.HttpException)ex).Response);

                StringBuilder errorDetails = new StringBuilder();
                if (error.Details != null && error.Details.Any())
                {
                    foreach (ErrorDetail errorDetail in error.Details)
                        errorDetails.AppendFormat("Field: {0}\nIssue: {1}\n", errorDetail.Field, errorDetail.Issue);
                }

                return new PaypalPayment
                {
                    Id = error.DebugId,
                    Message = String.Concat(error.Name, "\n\n", error.Message, "\n\nFurther Information:\n", errorDetails.ToString()),
                    CreatedTime = DateTime.Now,
                    Status = "error"
                };
            }
        }
    }
}
