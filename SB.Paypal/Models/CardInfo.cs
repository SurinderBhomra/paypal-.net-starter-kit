﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SB.Paypal.Enums;

namespace SB.Paypal.Models
{
    public class CardInfo
    {
        public string Number { get; set; }
        public int CVV2 { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpireYear { get; set; }
        public CardTypeEnum CardType { get; set; }
    }
}
