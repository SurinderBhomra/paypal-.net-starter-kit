﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.Paypal.Models
{
    public class PaypalPayment
    {
        public string Id { get; set; }
        public string InvoiceId { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
