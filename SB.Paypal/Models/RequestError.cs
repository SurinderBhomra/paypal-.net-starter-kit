﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SB.Paypal.Models
{
    public class RequestError
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("details")]
        public List<ErrorDetail> Details { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("information_link")]
        public string InformationLink { get; set; }

        [JsonProperty("debug_id")]
        public string DebugId { get; set; }
    }

    public class ErrorDetail
    {
        [JsonProperty("field")]
        public string Field { get; set; }

        [JsonProperty("issue")]
        public string Issue { get; set; }
    }
}
