﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SB.Paypal.Enums
{
    public enum CurrencyEnum
    {
        GBP = 1,
        USD = 2
    }
}
