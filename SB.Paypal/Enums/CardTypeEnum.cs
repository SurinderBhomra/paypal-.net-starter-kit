﻿namespace SB.Paypal.Enums
{
    public enum CardTypeEnum
    {
        NoCard = 0,
        Visa = 1,
        Mastercard = 2
    }
}