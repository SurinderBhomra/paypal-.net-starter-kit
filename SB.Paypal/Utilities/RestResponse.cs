﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SB.Paypal.Models;

namespace SB.Paypal.Utilities
{
    public class RestResponse
    {
        public static RequestError Error(string response)
        {
            return JsonConvert.DeserializeObject<RequestError>(response);
        }
    }
}
