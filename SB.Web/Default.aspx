﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pay by Card</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <fieldset>
            <legend>Payment by Card</legend>
            <ol>
                <li>
                    <label for="<%=PaymentCards.ClientID %>">Card Type</label>
                    <asp:DropDownList ID="PaymentCards" required runat="server">
                        <Items>
                            <asp:ListItem Value="">Select Card Type</asp:ListItem>
                            <asp:ListItem Value="Visa">Visa</asp:ListItem>
                            <asp:ListItem Value="Mastercard">Mastercard</asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </li>
                <li class="separator">
                    <label for="<%=CardNumber.ClientID %>">Card Number</label>
                    <asp:TextBox ID="CardNumber" runat="server" MaxLength="50" placeholder="Card Number" required></asp:TextBox>
                </li>
                <li class="col-1-2">
                    <label for="<%=ExpiryMonths.ClientID %>">Expiry Date</label>
                    <asp:DropDownList ID="ExpiryMonths" runat="server" required>
                        <Items>
                            <asp:ListItem Value="">Expiry Month</asp:ListItem>
                            <asp:ListItem Value="01">01 - Jan</asp:ListItem>
                            <asp:ListItem Value="02">02 - Feb</asp:ListItem>
                            <asp:ListItem Value="03">03 - Mar</asp:ListItem>
                            <asp:ListItem Value="04">04 - Apr</asp:ListItem>
                            <asp:ListItem Value="05">05 - May</asp:ListItem>
                            <asp:ListItem Value="06">06 - Jun</asp:ListItem>
                            <asp:ListItem Value="07">07 - Jul</asp:ListItem>
                            <asp:ListItem Value="08">08 - Aug</asp:ListItem>
                            <asp:ListItem Value="09">09 - Sept</asp:ListItem>
                            <asp:ListItem Value="10">10 - Oct</asp:ListItem>
                            <asp:ListItem Value="11">11 - Nov</asp:ListItem>
                            <asp:ListItem Value="12">12 - Dec</asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </li>
                <li class="col-2-2">
                    <asp:DropDownList ID="ExpiryYears" runat="server" required />
                </li>
                <li class="col-1-2 cvv">
                    <label for="<%=CVV.ClientID %>">CCV</label>
                    <asp:TextBox ID="CVV" runat="server" MaxLength="3" placeholder="CVV" required></asp:TextBox>
                </li>
            </ol>
        </fieldset>
        <br />
        <br />
        <asp:Button ID="ConfirmOrder" Text="Confirm Payment" runat="server" OnClick="ConfirmOrder_OnClick"/>
        <br />
        <br />
        <asp:Label ID="Output" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
