﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SB.Paypal;
using SB.Paypal.Enums;
using SB.Paypal.Models;

public partial class _Default : System.Web.UI.Page
{
    #region Form Properties

    private CardTypeEnum PaymentCardValue
    {
        get
        {
            if (Enum.IsDefined(typeof(CardTypeEnum), PaymentCards.SelectedValue))
            {
                CardTypeEnum cardType;
                Enum.TryParse(PaymentCards.SelectedValue, out cardType);

                return cardType;
            }
            else
            {
                return CardTypeEnum.NoCard;
            }
        }
    }

    private string CardNumberValue
    {
        get { return CardNumber.Text; }
    }

    private int ExpiryMonthValue
    {
        get
        {
            if (!String.IsNullOrEmpty(ExpiryMonths.SelectedValue))
                return int.Parse(ExpiryMonths.SelectedValue);
            else
                return 0;
        }
    }

    private int ExpiryYearValue
    {
        get
        {
            if (!String.IsNullOrEmpty(ExpiryYears.SelectedValue))
                return int.Parse(ExpiryYears.SelectedValue);
            else
                return 0;
        }
    }

    private string CVVValue
    {
        get { return CVV.Text; }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        PopulateExpiryDates();
    }

    private void PopulateExpiryDates()
    {
        ExpiryYears.Items.Add(new ListItem("Expiry Year", String.Empty));

        for (int y = DateTime.Now.Year; y <= DateTime.Now.AddYears(10).Year; y++)
            ExpiryYears.Items.Add(new ListItem(y.ToString(), y.ToString()));
    }

    protected void ConfirmOrder_OnClick(object sender, EventArgs e)
    {
        try
        {
            #region Customer Details

            PersonalDetails customerDetails = new PersonalDetails
            {
                FirstName ="Surinder",
                LastName = "Bhomra",
                EmailAddress = "surinder@me.com",
                Address1 = "123 Fake Street",
                Address2 = "Fake Area",
                City = "Oxford",
                CountryCode = "GB",
                County = "Oxforshire",
                PostCode = "OX123 5BA"
            };

            #endregion

            #region Products

            List<Product> products = new List<Product>();

            products.Add(new Product
            {
                Name = "Nexus 6",
                Price = 400,
                Quantity = 1,
                Sku = "NEXUS-6"
            });

            products.Add(new Product
            {
                Name = "Phone Case",
                Price = 10.50M,
                Quantity = 1,
                Sku = "PHONE-CASE-001"
            });

            products.Add(new Product
            {
                Name = "Phone Charger",
                Price = 5.50M,
                Quantity = 1,
                Sku = "PHONE-CHARGER"
            });

            #endregion

            // Attempt to send the card transaction to Paypal.
            PaypalPayment payment = SubmitOrderToPaypal("SURINDER-003", customerDetails, products); //Invoice ID needs to be unique per transaction

            if (payment != null && payment.Status == "success")
            {
                Output.Text = "Transaction successful.";
            }
            else
            {
                // Send error to user saying transaction could not be processed.
                Output.Text = "The transaction could not be processed.";
            }
        }
        catch (Exception ex)
        {
            // Send error to user saying transaction could not be processed.
            Output.Text = "The transaction could not be processed. Please check all your payment details and try again.";
        }
    }

    private PaypalPayment SubmitOrderToPaypal(string invoiceId, PersonalDetails customerDetails, List<Product> products)
    {
        CardInfo creditCard = new CardInfo
        {
            Number = CardNumberValue,
            CardType = PaymentCardValue,
            CVV2 = int.Parse(CVVValue),
            ExpiryMonth = ExpiryMonthValue,
            ExpireYear = ExpiryYearValue
        };

        CreditCardTransaction trans = new CreditCardTransaction(
            invoiceId,
            customerDetails,
            CurrencyEnum.GBP,
            products,
            creditCard
            );

        PaypalPayment output = trans.SendPayment();

        if (output.Status == "error")
            Output.Text = String.Format("Debug ID: {0}<br /><br />Error: {1}", output.Id, output.Message);

        return output;
    }
}